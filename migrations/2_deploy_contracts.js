const ABCoin= artifacts.require("ABCoin");
const Administration = artifacts.require("Administration");
const PauseOperation = artifacts.require("PauseOperation");

module.exports = function(deployer) {
  let _token;
  deployer
    .deploy(PauseOperation)
    .then(instance => {
      _token = instance.address;
      console.log("PauseOperation contract is created ", _token);
    });
  deployer
    .deploy(Administration)
    .then(instance => {
      _token = instance.address;
      console.log("Administration contract is created at ", _token);
    });
  deployer
    .deploy(ABCoin)
    .then(instance => {
      _token = instance.address;
      console.log("ABCoin contract is created ", _token);
    });
};
