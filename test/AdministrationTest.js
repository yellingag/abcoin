const Administration = artifacts.require("../contracts/Administration.sol");

let instance;

contract("Administration test", async accounts => {
  it("assign instances", async () => {
    instance = await Administration.deployed();
  });
  it("assign address to admin", async () => {
    let ownerAddress = await instance.owner();

    let success = await instance.addAdmin.sendTransaction(accounts[1], "admin2",
        { from: ownerAddress });
    assert.equal(success.receipt.status, true);

    let admin = await instance.admins(accounts[1]);
    assert.equal(admin.id, "admin2");
    assert.equal(admin.enabled, true);
  });
  it("check if admin", async () => {
    let success = await instance.isAdmin.call(accounts[1]);
    assert.equal(success, true);
  });
  it("remove admin", async () => {
    let ownerAddress = await instance.owner();

    let success = await instance.removeAdmin.sendTransaction(accounts[1],
        { from: ownerAddress });
    assert.equal(success.receipt.status, true);

    let admin = await instance.admins(accounts[1]);
    assert.equal(admin.id, "admin2");
    assert.equal(admin.enabled, false);
  });
});
