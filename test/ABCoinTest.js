const ABCoin = artifacts.require("../contracts/ABCoin.sol");
const Administration = artifacts.require("../contracts/Administration.sol");
const PauseOperation = artifacts.require("../contracts/PauseOperation.sol");

let coinInstance;
let adminInstance;
let pauseInstance;

let decimals=18;

contract("ABCoin test", async accounts => {
  it("can assign instances", async () => {
    coinInstance = await ABCoin.deployed();
    adminInstance = await Administration.deployed();
    pauseInstance = await PauseOperation.deployed();
  });
  it("can set associate contracts", async () => {
    // console.log(Administration.address);
    let setAdmin = await coinInstance.setAdministrationContract(
      Administration.address
    );
    let setPauseOp = await coinInstance.setPauseOperationContract(
      PauseOperation.address
    );
    assert.equal(setAdmin.receipt.status, true);
    assert.equal(setPauseOp.receipt.status, true);

    let setAdmin2 = await pauseInstance.setAdministrationContract(
      Administration.address
    );
    assert.equal(setAdmin2.receipt.status, true);
  });
  it("assign address to admin", async () => {
    let ownerAddress = await adminInstance.owner();

    let success = await adminInstance.addAdmin.sendTransaction(accounts[1], "admin2",
        { from: ownerAddress });
    assert.equal(success.receipt.status, true);

    let admin = await adminInstance.admins(accounts[1]);
    assert.equal(admin.id, "admin2");
    assert.equal(admin.enabled, true);
  });
  it("initialized fees receiver", async () => {
    let adminAddress = accounts[1];
    let admin = await adminInstance.admins(adminAddress);
    assert.equal(admin.enabled, true, "account 1 is not an admin");

    let success = await coinInstance.setFeesReceiver.sendTransaction(
      "0x77685437e762aaa90d4b6924aa9445c1198013f1",
      { from: adminAddress })
  });
  it("should have 0 ABCoin initialized", async () => {
    let balance = await coinInstance.getBalance.call(accounts[0]);
    assert.equal(balance.valueOf(), 0);
  });

  it("can issue coins to an address", async () => {
    let decimals = await coinInstance.getDecimals.call();

    let adminAddress = accounts[1];
    let admin = await adminInstance.admins(adminAddress);
    assert.equal(admin.enabled, true, "account 1 is not an admin");

    let tokens = 120*(10**10);

    let previousBalance = await coinInstance.getBalance.call(accounts[0], { from: adminAddress });
    let success = await coinInstance.issueCoins.sendTransaction(accounts[0],tokens, { from: adminAddress });
    let curBalance = await coinInstance.getBalance.call(accounts[0], { from: adminAddress });
    assert.equal(curBalance, previousBalance.toNumber() + tokens);
  });

  it("can get decimals from the contract", async () => {
    let decimals = await coinInstance.getDecimals.call();
    assert.ok(decimals>-1);
  });

  it("able to set fees receiver", async () => {
    let adminAddress = accounts[1];
    let admin = await adminInstance.admins(adminAddress);
    assert.equal(admin.enabled, true, "account 1 is not an admin");

    let testAddress = "0x243c569E16193167E77c4Cd5bbBfaA04AB958F4B";

    let success = await coinInstance.setFeesReceiver.call(testAddress, { from: adminAddress });
    assert.equal(success, true);
  });

  it("able to peer transfer coins", async() => {
    let adminAddress = accounts[1];
    let admin = await adminInstance.admins(adminAddress);
    assert.equal(admin.enabled, true, "account 1 is not an admin");

    let testAccount1 = accounts[2];
    let testAccount2 = accounts[3];

    let originalBalance1 = await coinInstance.getBalance.call(testAccount1, { from: testAccount1 });
    let originalBalance2 = await coinInstance.getBalance.call(testAccount2, { from: testAccount2 });

    let acc1tokens = 150*(10**10);
    let acc2tokens = 90*(10**10);

    let amountToTransfer = 20*(10**10);

    await coinInstance.issueCoins.sendTransaction(testAccount1,acc1tokens,
      { from: adminAddress });
    await coinInstance.issueCoins.sendTransaction(testAccount2,acc2tokens,
      { from: adminAddress });

    success = await coinInstance.transfer.sendTransaction(testAccount2,amountToTransfer,
      { from: testAccount1 });

    let balance1 = await coinInstance.getBalance.call(testAccount1, { from: testAccount1 });
    let balance2 = await coinInstance.getBalance.call(testAccount2, { from: testAccount2 });

    let fees = (amountToTransfer*2)/100;

    assert.equal(balance1.toNumber(),
      originalBalance1.toNumber() + acc1tokens - fees - amountToTransfer);
    assert.equal(balance2.toNumber(),
      originalBalance2.toNumber() + acc2tokens + amountToTransfer);

  });
  it("able to force transfer", async() => {
    let adminAddress = accounts[1];
    let admin = await adminInstance.admins(adminAddress);
    assert.equal(admin.enabled, true, "account 1 is not an admin");

    let testAccount1 = accounts[5];
    let testAccount2 = accounts[6];

    let originalBalance1 = await coinInstance.getBalance.call(testAccount1, { from: testAccount1 });
    let originalBalance2 = await coinInstance.getBalance.call(testAccount2, { from: testAccount2 });

    let acc1tokens = 150*(10**10);
    let acc2tokens = 90*(10**10);

    let amountToTransfer = 200;

    await coinInstance.issueCoins.sendTransaction(testAccount1,acc1tokens,
      { from: adminAddress });
    await coinInstance.issueCoins.sendTransaction(testAccount2,acc2tokens,
      { from: adminAddress });

    success = await coinInstance.forceTransfer.sendTransaction(
      testAccount1, testAccount2, amountToTransfer,
      { from: adminAddress });

    let balance1 = await coinInstance.getBalance.call(testAccount1, { from: testAccount1 });
    let balance2 = await coinInstance.getBalance.call(testAccount2, { from: testAccount2 });

    let fees = 0;

    assert.equal(balance1.toNumber(),
      originalBalance1.toNumber() + acc1tokens - amountToTransfer - fees );
    assert.equal(balance2.toNumber(),
      originalBalance2.toNumber() + acc2tokens + amountToTransfer);
  });
  it("able to redeem", async () => {
    let adminAddress = accounts[1];
    let testAccount1 = accounts[5];
    let acc1tokens = 150*(10**10);
    let originalBalance1 = await coinInstance.getBalance.call(testAccount1, { from: testAccount1 });
    await coinInstance.issueCoins.sendTransaction(testAccount1,acc1tokens,
      { from: adminAddress });
    let balance1 = await coinInstance.getBalance.call(testAccount1, { from: testAccount1 });
    assert.equal(balance1.toNumber(), originalBalance1.toNumber() + acc1tokens);

    await coinInstance.redeem.sendTransaction(testAccount1,
      { from: adminAddress });
    let redeemBalance = await coinInstance.getBalance.call(testAccount1, { from: testAccount1 });
    assert.equal(redeemBalance,0);
  });
});
