const Administration = artifacts.require("../contracts/Administration.sol");
const PauseOperation = artifacts.require("../contracts/PauseOperation.sol");

let adminInstance;
let instance;

contract("PauseOperation test", async accounts => {
  it("assign instances", async () => {
    adminInstance = await Administration.deployed();
    instance = await PauseOperation.deployed();
  });
  it("set associate contracts", async () => {
    let setAdmin = await instance.setAdministrationContract(
      Administration.address
    );
    assert.equal(setAdmin.receipt.status, true);
  });
  it("assign address to admin", async () => {
    let ownerAddress = await adminInstance.owner();

    let success = await adminInstance.addAdmin.sendTransaction(accounts[1], "admin2",
        { from: ownerAddress });
    assert.equal(success.receipt.status, true);

    let admin = await adminInstance.admins(accounts[1]);
    assert.equal(admin.id, "admin2");
    assert.equal(admin.enabled, true);
  });
  it("pause operation", async () => {
    let adminAddress = accounts[1];
    let success = await instance.setPause.sendTransaction(true,
      { from: adminAddress });
    let _isPaused = await instance.isPaused.call();
    assert.equal(_isPaused, true);

    success = await instance.setPause.sendTransaction(false,
      { from: adminAddress });
    _isPaused = await instance.isPaused.call();
    assert.equal(_isPaused, false);
  });
});
