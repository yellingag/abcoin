pragma solidity >=0.4.21 <0.6.0;

contract Administration {
  // ----------------------------------------------------------------------------
  // Able to extend more admin features
  // ----------------------------------------------------------------------------
  struct Admin {
    string id;
    bool enabled;
  }

  mapping (address => Admin) public admins;
  address public owner;

  event AddedAdminEvent(
    string indexed _adminId,
    address indexed _adminAddress,
    address indexed _requestor
  );

  event RemovedAdminEvent(
    string indexed _adminId,
    address indexed _adminAddress,
    address indexed _requestor
  );

  constructor() public {
    owner = msg.sender;
  }

  function isAdmin(address _admin) public view returns (bool) {
    return admins[_admin].enabled;
  }

  function addAdmin(address _address, string memory _id) public returns (bool) {
    require(owner==msg.sender, "Sender doesn't have sufficient permission");
    emit AddedAdminEvent(_id, _address, msg.sender);
    admins[_address]=Admin(_id, true);

    return true;
  }

  function removeAdmin(address _address) public returns (bool) {
    require(owner==msg.sender, "Sender doesn't have sufficient permission");
    Admin storage admin = admins[_address];
    admin.enabled = false;
    emit RemovedAdminEvent(admin.id, _address, msg.sender);
    return true;
  }
}
