pragma solidity >=0.4.21 <0.6.0;

import "./PauseOperation.sol";
import "./Administration.sol";

// ---------------------------------------------------------
// Safe maths
// ---------------------------------------------------------
library SafeMath {
  function add(uint a, uint b) internal pure returns (uint c) {
    c = a + b;
    require(c >= a, "overflow");
  }
  function sub(uint a, uint b) internal pure returns (uint c) {
    require(b <= a, "negative value");
    c = a - b;
  }
  function mul(uint a, uint b) internal pure returns (uint c) {
    c = a * b;
    require(a ==0 || c / a == b, "overflow");
  }
  function div(uint a, uint b) internal pure returns (uint c) {
    require(b > 0,"negative value");
    c = a / b;
  }
}

contract ABCoin {
  using SafeMath for uint;

  address public owner;
  string public name;
  string public symbol;
  address feesAccount;
  uint8 public decimals = 18;

  Administration adminContract;
  PauseOperation pauseOpContract;

  mapping (address => uint) balances;

  event IssuedCoinsToAddressEvent(
    address indexed _to,
    uint _amount,
    address indexed _issuer
  );
  event FeesReceiverEventSetEvent(
    address indexed _oldReceiver,
    address indexed _newReceiver,
    address indexed _admin
  );
  event TokenTransferredEvent(
    address indexed _from,
    address indexed _to,
    uint amount,
    uint fees
  );
  event TokenTransferredRequestFromAdminEvent(
    address indexed _from,
    address indexed _to,
    uint amount,
    address indexed _admin
  );
  event RedeemEvent(
    address indexed _address,
    uint amount,
    address indexed _admin
  );

  constructor() public {
    owner = msg.sender;
    name = "ABCoin";
    symbol = "ABC";
  }

  function setAdministrationContract(address _address) public returns (bool) {
    require(owner==msg.sender, "Insufficient permission");
    adminContract = Administration(_address);
    return true;
  }

  function setPauseOperationContract(address _address) public returns (bool) {
    require(owner==msg.sender, "Insufficient permission");
    pauseOpContract = PauseOperation(_address);
    return true;
  }

  function issueCoins(address _to, uint _amount) public returns (bool) {
    _isOperationPaused();
    bool isAdmin = adminContract.isAdmin(msg.sender);
    require(isAdmin, "Issuer is not admin");
    balances[_to] = balances[_to].add(_amount);
    emit IssuedCoinsToAddressEvent(_to, _amount, msg.sender);
    return true;
  }

  function getDecimals() public view returns (uint8) {
    return decimals;
  }

  function getBalance(address _coinOwner) public view returns (uint) {
    _isOperationPaused();
    bool isAdmin = adminContract.isAdmin(msg.sender);
    require(isAdmin || _coinOwner==msg.sender, "Insufficient permission");
    return balances[_coinOwner];
  }

  function setFeesReceiver(address _feesAccount) public returns (bool) {
    bool isAdmin = adminContract.isAdmin(msg.sender);
    require(isAdmin, "Insufficient permission");
    feesAccount = _feesAccount;
    emit FeesReceiverEventSetEvent(feesAccount, _feesAccount, msg.sender);
    return true;
  }

  function redeem(address _address) public returns (bool) {
    _isOperationPaused();
    bool isAdmin = adminContract.isAdmin(msg.sender);
    require(isAdmin, "Insufficient permission");
    uint _balance = balances[_address];
    emit RedeemEvent(_address, _balance, msg.sender);
    balances[_address] = 0;
    return true;
  }

  function transfer(address _to, uint _amount) public returns (bool) {
    _isOperationPaused();
    return _transfer(msg.sender, _to, _amount, 2);
  }

  function forceTransfer(address _from, address _to, uint _amount) public
      returns (bool) {
    bool isAdmin = adminContract.isAdmin(msg.sender);
    require(isAdmin, "Requestor is not admin");
    emit TokenTransferredRequestFromAdminEvent(_from, _to, _amount, msg.sender);
    return _transfer(_from, _to, _amount, 0);
  }

  function _transfer(address _from, address _to, uint _amount, uint _feesPercent)
      internal returns (bool) {
    uint fees = (_amount.mul(_feesPercent)).div(100);
    uint totalAmount = _amount.add(fees);
    require(_from != _to, "Origin and destination cannot be the same address");
    require(balances[_from] >= totalAmount, "Balance not enough");
    uint expectedSum = balances[_from].add(balances[_to]).sub(fees);
    uint previousFees = balances[feesAccount];

    balances[_from] = balances[_from].sub(totalAmount);
    balances[_to] = balances[_to].add(_amount);
    balances[feesAccount] = balances[feesAccount].add(fees);
    assert(balances[_from].add(balances[_to]) == expectedSum);
    assert(previousFees.add(fees) == balances[feesAccount]);
    emit TokenTransferredEvent(_from, _to, _amount, fees);
    return true;
  }

  function _isOperationPaused() internal view returns (bool) {
    bool _paused = pauseOpContract.isPaused();
    require(_paused == false, "Operations is being paused");
  }

}
