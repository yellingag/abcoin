pragma solidity >=0.4.21 <0.6.0;

import "./Administration.sol";

contract PauseOperation {
  bool public paused;
  address public owner;

  Administration adminContract;

  constructor() public {
    owner = msg.sender;
    paused = false;
  }

  function isPaused() public view returns (bool) {
    return paused;
  }

  function setPause(bool _pause) public returns (bool) {
    bool isAdmin = adminContract.isAdmin(msg.sender);
    require(isAdmin, "Insufficient permission");
    paused = _pause;
  }

  function setAdministrationContract(address _address) public returns (bool) {
    require(owner==msg.sender, "Insufficient permission");
    adminContract = Administration(_address);
    return true;
  }
}
